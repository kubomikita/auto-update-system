<?php
//header('Content-Type: text/html; charset=utf-8');
$updates=array();
$updatesArchive = array();


$version = $_GET["version"];
if($version != null){

    $keys = explode("\n",file_get_contents("keys.dat"));


    foreach(glob('*.zip') as $package){

        $zip = zip_open($package);

        if ($zip) {
            while ($zip_entry = zip_read($zip)) {
                if(zip_entry_name($zip_entry) == "readme.txt"){

                    if (zip_entry_open($zip, $zip_entry, "r")) {

                        $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

                        list($ver,$desc,$price) = explode("\n",$buf);

                        zip_entry_close($zip_entry);
                    }

                }
            }

            zip_close($zip);

        }
        $ver =  str_replace("\r","",$ver);
        $desc = str_replace("\r","",$desc);
        if((float) $version < (float) $ver ){
            $updates[$ver]["desc"] = $desc;
            $updates[$ver]["price"] = (float) $price;
            if($price == 0 or (in_array($_GET["key"],$keys) and $_GET["key"]!="")){
                $updatesArchive[$ver] = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"]."".str_replace("/latest.php","",$_SERVER["SCRIPT_NAME"])."/".$package;
                $updates[$ver]["zip"] = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"]."".$_SERVER["SCRIPT_NAME"]."?download=".$ver."&version=".$version;
                if(in_array($_GET["key"],$keys)){
                    $updates[$ver]["zip"] .= '&key='.$_GET["key"];
                }
            }
        }
    }

} else {
    $updates["error"] = "Please, send us your version of app.";
}
if($_GET["download"]){
    $file_url = $updatesArchive[$_GET["download"]];
    if($updates[$_GET["download"]]["price"] > 0){
        if(!in_array($_GET["key"],$keys)){
            exit;
        }
    }
    //var_Dump($file_url);
    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
    readfile($file_url);
    exit;
}

//var_dump($_SERVER);
//var_dump( 1.00 < 1.01,$updates);
header('Content-Type: application/json; charset=utf-8');
echo json_encode($updates);
