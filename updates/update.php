<h1>Auto update system</h1>
<?
header('Content-Type: text/html; charset=utf-8');
ini_set('max_execution_time',60);

function file_get_contents_curl($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);       

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

$VERSION = file_get_contents("version");
$APPDIR = "../";
$VERSIONS_API = 'http://localhost/auto-update-system/checkUpdates/latest.php?version='.$VERSION;
if($_GET["key"] != ""){
    $VERSIONS_API .= "&key=".$_GET["key"];
}
$EXCLUDED = explode("\n",file_get_contents("excluded"));
//var_dump($EXCLUDED);
//var_dump($_ENV['site']['files']['includes-dir']);
//var_dump('http://localhost/auto-update-system/checkUpdates/latest.php?version='.$VERSION);
//Check for an update. We have a simple file that has a new release version on each line. (1.00, 1.02, 1.03, etc.)
$getVersions = file_get_contents_curl($VERSIONS_API) or die ('ERROR');
//var_dump($getVersions);exit;
$UPDATE = true;
if ($getVersions != '')
{
    //If we managed to access that file, then lets break up those release versions into an array.
    echo '<p>CURRENT VERSION: '.$VERSION.'</p>';
    echo '<p>Reading Current Releases List</p>';
    $versionList = (array) json_decode($getVersions);
    foreach ($versionList as $newV=> $aV)
    {
        if ( $newV > $VERSION) {
            echo '<p>New Update Found: <strong>v'.$newV.'</strong> <br>Description: <strong>'.$aV->desc.'</strong><br>Price: <strong>'.$aV->price.' €</strong></p>';
            $found = true;
           //var_dump($aV);
            //Download The File If We Do Not Have It
            if ( !is_file( "v".$newV.".zip" )) {
                echo '<p>Downloading New Update</p>';
                if($aV->zip != null){
                    $newUpdate = file_get_contents_curl($aV->zip);
                    //var_dump($av->zip,$newUpdate);
                    file_put_contents("v".$newV.".zip",$newUpdate);
                    echo '<p>Update Downloaded And Saved</p>';
                } else {
                    $UPDATE = false;
                    echo '<p>You not have a permission to download update.</p>';
                    //exit;
                }
                //if ( !is_dir( $_ENV['site']['files']['includes-dir'].'/UPDATES/' ) ) mkdir ( $_ENV['site']['files']['includes-dir'].'/UPDATES/' );
                /*$dlHandler = fopen($_ENV['site']['files']['includes-dir'].'/UPDATES/MMD-CMS-'.$newV.'.zip', 'w');
                if ( !fwrite($dlHandler, $newUpdate) ) { echo '<p>Could not save new update. Operation aborted.</p>'; exit(); }
                fclose($dlHandler);*/


            } else echo '<p>Update already downloaded.</p>';    
           
            if ($_GET['doUpdate'] == true and $UPDATE) {
                //Open The File And Do Stuff
                ob_start();
                $zipHandle = zip_open('v'.$newV.'.zip');
                echo '<ul>';
                while ($aF = zip_read($zipHandle) )
                {
                    $thisFileName = zip_entry_name($aF);
                    $thisFileDir = dirname($thisFileName);
                    //var_dump($thisFileName,$EXCLUDED,);
                    if(!in_array($thisFileName,$EXCLUDED)){
                        //var_dump($thisFileName,$thisFileDir,substr($thisFileName,-1,1));
                        //Continue if its not a file
                        if ( substr($thisFileName,-1,1) == '/') continue;

                        //var_dump($APPDIR.''.$thisFileDir,$thisFileDir);
                        //Make the directory if we need to...
                        if ( !is_dir ( $APPDIR.''.$thisFileDir ) )
                        {
                             mkdir ( $APPDIR.''.$thisFileDir );
                             echo '<li>Created Directory '.$thisFileDir.'</li>';
                        }

                        //Overwrite the file
                        if ( !is_dir($APPDIR.$thisFileName) and $thisFileName != "readme.txt" ) {
                            echo '<li>'.$thisFileName.'...........';
                            $contents = zip_entry_read($aF, zip_entry_filesize($aF));
                            $contents = str_replace("rn", "n", $contents);
                            $updateThis = '';
                            //if() continue;
                            //If we need to run commands, then do it.
                            if(substr($thisFileName,-3) == "sql"){
                                echo " TODO: execute to MySQL</li>";

                            } else {
                                $updateThis = fopen($APPDIR.$thisFileName, 'w');
                                fwrite($updateThis, $contents);
                                fclose($updateThis);
                                unset($contents);
                                echo' UPDATED</li>';
                            }
                        }
                    } else {
                        echo '<li style="color:red;">'.$thisFileName.' was EXCLUDED from update.</li>';
                    }
                }
                echo '</ul>';
                $log = ob_get_contents();
                ob_end_clean();
                $log = "<h3>App updated to v$newV at ".date("d.m.Y H:i:s")."</h3>".$log;
                file_put_contents("lastupdate.log",$log);
                $updated = TRUE;
            } else if($UPDATE == false){
                echo '<p>If you want to buy update, please call +421 911 531 872 to get update Code.</p><p><form method="get">code: <input type="text" name="key"> <input type="submit"></form></p>';
            }
            else echo '<p>Update ready. <a href="?doUpdate=true">&raquo; Install Now?</a></p>';
            break;
        }
    }
    
    if ($updated == true){
        //set_setting('site','CMS',$newV);
        file_put_contents("version",$newV);
        @unlink(dirname(__FILE__)."/v".$newV.".zip");
        //echo '<p class="success">&raquo; CMS Updated to v'.$newV.'</p>';
        header("location:update.php?success=1");
    } elseif ($found != true) {
        echo '<p>&raquo; No update is available.</p>';
    }

    if($_GET["success"]){
        echo file_get_contents("lastupdate.log");

    }
    echo '<a href="update.php">Check for updates</a>';
}
else echo '<p>Could not find latest realeases.</p>';
